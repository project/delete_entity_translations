<?php

namespace Drupal\delete_entity_translations\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form.
 */
class DeleteEntityTranslationsForm extends FormBase {

  /**
   * The batch default size.
   */
  const BATCH_SIZE = 50;

  /**
   * The language manager service.
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The Entity type manager service.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * OrphanedCompositeEntitiesDeleteForm constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The EntityTypeManager service.
   */
  final public function __construct(LanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_manager) {
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_entity_translations_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['langcode'] = [
      '#title' => $this->t('Language'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $this->getLanguages(),
      '#description' => $this->t('Select the language'),
    ];

    $form['entity_types'] = [
      '#title' => $this->t('Entity types'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#options' => $this->getEntityTypes(),
      '#description' => $this->t('Select the entity types'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#required' => TRUE,
      '#value' => $this->t('Delete entities / translations'),
    ];

    return $form;
  }

  /**
   * Get list of entity types with selected language | translation.
   *
   * @return array
   *   Array of entity types with selected language | translation.
   */
  public function getEntityTypes(): array {
    $entity_types = [];
    $definitions = $this->entityTypeManager->getDefinitions();

    foreach ($definitions as $id => $definition) {
      $entity_types[$id] = $definition->getLabel();
    }
    asort($entity_types);
    return $entity_types;
  }

  /**
   * Get list of available languages.
   *
   * @return array
   *   Array of languages with langcodes as keys.
   */
  public function getLanguages(): array {
    $languages = $this->languageManager->getLanguages();
    $langcodes = [];

    foreach ($languages as $key => $language) {
      $langcodes[$key] = $language->getName();
    }

    return $langcodes;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $langcode = $form_state->getValue('langcode');
    $entity_types = $form_state->getValue('entity_types');
    $this->runDeleteEntityTranslations($langcode, $entity_types);
  }

  /**
   * Gets list of all entity and  passes to batch.
   *
   * @param string $langcode
   *   The langcode of selected language.
   * @param array $entity_types
   *   The list of entity types need to process.
   */
  public function runDeleteEntityTranslations(string $langcode, array $entity_types) {
    $operations = [];

    foreach ($entity_types as $entity_type) {
      $operations[] = [
        [$this, 'processItems'],
        [
          $langcode,
          $entity_type,
        ],
      ];
    }

    $batch = [
      'operations' => $operations,
      'finished' => [$this, 'finished'],
      'title' => $this->t('Delete selected entities / translations'),
      'init_message' => $this->t('The process is starting.'),
      'progress_message' => $this->t('Processed @current out of @total operations.'),
      'error_message' => $this->t('Batch has encountered an error.'),
    ];
    batch_set($batch);
  }

  /**
   * Batch Callback to generate csv.
   *
   * @param string $langcode
   *   The langcode of selected language.
   * @param string $entity_type
   *   The entity type need to process.
   * @param array $context
   *   The batch context.
   */
  public function processItems(string $langcode, string $entity_type, array &$context): void {
    $query = $this->entityTypeManager->getStorage($entity_type)->getQuery();
    $context['results']['processed'] = $context['results']['processed'] ?? 0;

    if (empty($context['sandbox'])) {
      $count = $query
        ->accessCheck(FALSE)
        ->condition('langcode', $langcode)
        ->count()
        ->execute();

      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = $count;
    }

    $ids = $query
      ->accessCheck(FALSE)
      ->condition('langcode', $langcode)
      ->range(0, self::BATCH_SIZE)
      ->execute();

    if (!empty($ids)) {
      foreach ((array) $ids as $id) {

        $entity = $this->entityTypeManager->getStorage($entity_type)->load($id);

        if ($entity instanceof TranslatableInterface) {
          if (!$entity->isDefaultTranslation()) {
            $entity = $entity->getUntranslated();
          }

          if ($entity->language()->getId() === $langcode) {
            $entity->delete();
          }
          elseif ($entity->hasTranslation($langcode)) {
            $entity->removeTranslation($langcode);
            $entity->save();
          }
        }

        // Update our progress information.
        $context['sandbox']['progress']++;
        $context['results']['processed']++;
        $context['message'] = $this->t('@progress out of @total total items have been processed.', [
          '@progress' => $context['sandbox']['progress'],
          '@total' => $context['sandbox']['max'],
        ]);
      }
    }

    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Batch finished callback.
   *
   * @param bool $success
   *   Was the process successful?
   * @param array $results
   *   Batch process results array.
   * @param array $operations
   *   Performed operations array.
   */
  public function finished(bool $success, array $results, array $operations) {
    $message = $this->t('Deleted @count entities or their translations', [
      '@count' => $results['processed'],
    ]);

    $this->messenger()
      ->addStatus($message);
  }

}
