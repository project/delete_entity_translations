<?php

namespace Drupal\Tests\delete_entity_translations\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for deleting of entities with selected languages or their translations.
 *
 * @group delete_entity_translations
 */
class DeleteEntityTranslationsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_translation',
    'delete_entity_translations',
    'language',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalCreateContentType(['type' => 'page']);
    $admin = $this->drupalCreateUser([
      'access administration pages',
      'administer content translation',
      'administer content types',
      'administer languages',
      'create content translations',
      'create page content',
      'delete entity translations',
      'edit any page content',
      'translate any entity',
    ]);
    $this->drupalLogin($admin);

    // Add predefined language and verify that language was added successfully.
    $edit = [
      'predefined_langcode' => 'pl',
    ];
    $this->drupalGet('admin/config/regional/language/add');
    $this->submitForm($edit, 'Add language');
    $this->assertSession()->pageTextContains('Polish');

    // Set path prefixes for both languages.
    $this->config('language.negotiation')->set('url', [
      'source' => 'path_prefix',
      'prefixes' => [
        'en' => 'en',
        'pl' => 'pl',
      ],
    ])->save();

    // Enable translation for 'page' type nodes.
    $edit = [
      'entity_types[node]' => 1,
      'settings[node][page][translatable]' => 1,
    ];
    $this->drupalGet('admin/config/regional/content-language');
    $this->submitForm($edit, 'Save configuration');

    // Create a node and it's translation.
    $node = $this->drupalCreateNode(['type' => 'page']);
    $this->drupalGet('node/' . $node->id() . '/translations');
    $this->clickLink('Add');
    $this->submitForm([], 'Save (this translation)');
  }

  /**
   * Test deleting of entities with selected languages or their translations.
   */
  public function testDeleteEntityTranslations() {
    // Check the count of nodes with 'pl' translation before and after deleting.
    $this->assertSame(1, $this->getNodesCount('pl'));
    $this->drupalGet('admin/config/regional/delete-entity-translations');
    $edit = [
      'langcode' => 'pl',
      'entity_types[]' => 'node',
    ];
    $this->submitForm($edit, 'Delete entities / translations');
    $this->assertSame(0, $this->getNodesCount('pl'));

    // Check the count of nodes with 'en' original language before and after
    // deleting.
    $this->assertSame(1, $this->getNodesCount('en'));
    $this->drupalGet('admin/config/regional/delete-entity-translations');
    $edit = [
      'langcode' => 'en',
      'entity_types[]' => 'node',
    ];
    $this->submitForm($edit, 'Delete entities / translations');
    $this->assertSame(0, $this->getNodesCount('en'));
  }

  /**
   * Get count of nodes for selected language.
   *
   * @param string $langcode
   *   The langcode of node.
   *
   * @return int
   *   The count of nodes for selected language.
   */
  public function getNodesCount(string $langcode): int {
    return \Drupal::entityQuery('node')
      ->condition('type', 'page')
      ->accessCheck(FALSE)
      ->condition('langcode', $langcode)
      ->count()
      ->execute();
  }

}
