# Delete Entity Translations

Deleting a language will remove all interface translations associated
with it, but content in this language will be set to be language neutral
and will not be deleted. The Delete Entity Translations module allow to
delete entities and their translations for selected language before removing
the language itself.

## Installation

#### Install without composer

Download the zip or tgz archive of the latest release from the [project page](https://www.drupal.org/project/delete_entity_translations).
Extra the archive and move the folder to the site's `modules/contrib` directory.

#### Install using composer

`composer require 'drupal/delete_entity_translations'`

## How to use

Go to `admin/config/regional/delete-entity-translations`, select the language with entity types you want to delete and run the process of deleting entities / translations. Here more detailed [documentation](https://www.drupal.org/docs/contributed-modules/delete-entity-translations) with screens.

## Maintainer

Alexander Shabanov (fromme) - https://www.drupal.org/u/fromme
